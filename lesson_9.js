var studentsAndPoints = ['Алексей Петров', 0, 'Ирина Овчинникова', 60, 'Глеб Стукалов', 30, 'Антон Павлович', 30, 'Виктория Заровская', 30, 'Алексей Левенец', 70, 'Тимур Вамуш', 30, 'Евгений Прочан', 60, 'Александр Малов', 0];

// Задание 1. Создание конструктора Student.
console.log('\nЗадание 1. Создание конструктора Student.');

// Конструктор  Student
function Student(nameSt, pointsSt) {
	this.name = nameSt;
	this.points = pointsSt;
}

Student.prototype.show = function () {
    console.log('Студент %s набрал %d баллов', this.name, this.points);
}

// Задание 2. Создание конструктора StudentList.
console.log('\nЗадание 2. Создание конструктора StudentList.');
function StudentList(groupName, stdntAndPnt) {
	console.log('\nСоздана группа %s', groupName);
    
    // список студентов хранится в локальном массиве объекта StudentList
	var s = [];
    this.students = s;
    
	if(stdntAndPnt !== undefined) {
		stdntAndPnt.forEach(function (item,i,arr) {
			if(!(i%2))
				s.push(new Student(arr[i], arr[i+1]));
		});
	}
	else console.log('Массив студентов пустой!');
	
    // название группы
	this.name = groupName;
}

// метод добавления студентов
StudentList.prototype.add = function () {
    // копирование элементов псевдомассива arguments в реальный массив 
    var s = this.students;
    var args = [].slice.call(arguments);
	args.forEach(function(item, i, arr){
		if(!(i%2))
            s.push(new Student(arr[i], arr[i+1]));
	});
};

// возвращение объекта 'Student' по имени или по количеству баллов
StudentList.prototype.getStudent = function (arg) {
	var student;
    
    // поиск студента по имени
    if(typeof(arg) === 'string'){
        student = this.students.find(function(item,i,arr){
            return item.name === arg;
        });
        this.students.splice(this.students.indexOf(student), 1);
    }
    
    // поиск студента по количеству баллов
    if(typeof(arg) === 'number') {
        student = this.students.find(function(item,i,arr){
            return item.points === arg;
        });
    }
    
	return student;
};

//возврат объекта "студент" по индексу
StudentList.prototype.get = function (i) {
	return this.students[i];
};

// метод подсчета количества студентов в группе
StudentList.prototype.length = function () {
    return this.students.length;
};

// Мой вывод списка студентов для отладки
StudentList.prototype.myShow = function () {
	this.students.forEach(function (item) {
		console.log('Студент %s набрал %d баллов', item.name, item.points);
	});
};

// метод добавления студента в группу
StudentList.prototype.pushStudent = function () {
	this.students.push(arguments[0]);
}

// Задание 3. Создание списка студентов группы HJ-2.
console.log('\nЗадание 3. Создание списка студентов группы HJ-2.');
var hj2 = new StudentList('HJ-2', studentsAndPoints);
hj2.myShow();

// Задание 4. Добавление новых студентов.
console.log('\nЗадание 4. Добавление новых студентов.');
hj2.add('Иван Иваныч', 10, 'Семен Семеныч', 0, 'Петр Петрович', 30);
console.log('Добавлены новые студенты в группу HJ-2:');
hj2.myShow();

// Задание 5. Создание группы HTML-7 и добавлние студентов.
console.log('\nЗадание 5. Создание группы HTML-7 и добавлние студентов.');
var html7 = new StudentList('HTML-7');
html7.myShow();

// добавление студентов
html7.add('Лариса Ларисовна', 10, 'Антон Антонович', 0, 'Турсумбек', 30);
console.log('Добавлены новые студенты в группу HTML-7:');
html7.myShow();

// Задание 6. Метод show().
console.log('\nЗадание 6. Метод show().');
StudentList.prototype.show = function () {
	console.log('\nГруппа %s (%d студентов)', this.name, this.length());
	this.students.forEach(function(item){
        console.log('Студент %s набрал %d баллов', item.name, item.points);
    });
};

hj2.show();
html7.show();

// Задание 7. Перевод студента из группы HJ-2 в HTML-7
console.log('\nЗадание 7. Перевод студента из группы HJ-2 в HTML-7');
html7.pushStudent(hj2.getStudent('Иван Иваныч'));
hj2.show();
html7.show();

// Дополнительное задание. Возвращение студента с наибольшим баллом в этой группе.
console.log('\nДополнительное задание. Возвращение студента с наибольшим баллом в этой группе.');

// переопределение метода valueOf для StudentList
StudentList.prototype.valueOf = function () {
    var porintsArray=[];
    this.students.forEach(function (item,i,arr) {
        porintsArray.push(item.points);
    });
    return porintsArray;
};

// Реализация метода 'max'
StudentList.prototype.max = function (){
    return this.getStudent(Math.max.apply(null, this.valueOf()));
};

var maxStudent = hj2.max();
console.log('Студент с максимальным баллом %d в группе HJ-2: %s', maxStudent.points, maxStudent.name);
maxStudent = html7.max();
console.log('Студент с максимальным баллом %d в группе HJ-2: %s', maxStudent.points, maxStudent.name);